<?php

namespace App\Http\Controllers\contacto;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
class ContactoController extends Controller
{
    public function viewContacto(Request $request){
        return view('contenido.contacto');
    }
    
    /*Enviar correo*/
    public function send(Request $request){
        if($request->ajax()){
            
             $data=[
                'nombre'=>$request->nombre,
                'correo'=>$request->correo,
                'asunto'=>$request->asunto,
                'mensaje'=>$request->mensaje
                ];
                Mail::send('mail.mail-send',$data,function($message) use ($data){
                    $message->to('autodelsureste@gmail.com');
                    $message->subject($data['asunto']);
                });
            
            return 'Mensaje enviado correctamente ';
        }
    }
}
