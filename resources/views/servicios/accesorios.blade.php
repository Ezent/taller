@extends('template.welcome')
@section('navbar')
@parent
@endsection
@section('carousel')
@endsection
@section('section1')
@endsection
@section('section2')
@endsection
@section('section3')
  <section class="latest_news_area p_100">
            <div class="container">
                <div class="b_center_title">
                    <h2>Accesorios</h2>
                    <!--<p></p>-->
                </div>
                <div class="l_news_inner">
                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <div class="l_news_item">
                                <div class="l_news_img"><a href="#"><img class="img-fluid" src="{{asset('img/servicios/soporte_Magnetico.jpg')}}" alt=""></a></div>
                                <div class="l_news_content">
                                    <a href="#"><h4>Soporte Magnético</h4></a>
                                    <p class="text-justify">El soporte magnético scosche para tu auto es un sistema de montaje para smartphones elegante y versátil, 
                                        utiliza imanes de alta potencia para asegurar el dispositivo y que así quede inmovilizado firmemente en su lugar.
                                        En la carretera, en la oficina, en casa, en todas partes.</p>
                                    <!--<a class="more_btn" href="#">Learn More</a>-->
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="l_news_item">
                                <div class="l_news_img"><a href="#"><img class="img-fluid" src="{{asset('img/servicios/arrancador.jpg')}}" alt=""></a></div>
                                <div class="l_news_content">
                                    <a href="#"><h4>Arrancador para auto</h4></a>
                                    <p class="text-justify">
                                    Este dispositivo te permitirá arrancar tu automóvil si es que su batería se descargó. Por su tamaño ultra compacto podrás llevarlo 
                                    en todos tus viajes, sin sacrificar espacio en tu coche. Es muy sencillo de utilizar; lo único que debes hacer para pasar corriente 
                                    a la batería es conectar en sus polos los caimanes (incluidos) e insertar el conector de éstos en el arrancador, para luego 
                                    dar marcha al auto. 
                                    </p>
                                    <!--<a class="more_btn" href="#">Learn More</a>-->
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="l_news_item">
                                <div class="l_news_img"><a href="#"><img class="img-fluid" src="{{asset('img/servicios/gps.jpg')}}" alt=""></a></div>
                                <div class="l_news_content">
                                    <a href="#"><h4>GPS</h4></a>
                                    <p class="text-justify">Trackimo utiliza el GPS y la tecnología celular para proteger a los que mas te importan. Un dispositivo 
                                        de localización personal el cual es pequeño (cabe en la palma de tu mano) pero lo suficientemente potente para lograr un 
                                        seguimiento de tus seres mas queridos. Al adquirir el dispositivo se te brinda un año completamente gratis en el servicio
                                        de Voz y Datos, para que disfrutes completamente tu localizador desde el primer día.</p>
                                    <!--<a class="more_btn" href="#">Learn More</a>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection

<!-- Pie de pagina por default-->
@section('section4')
@parent
@endsection
@section('footer')
@parent
@endsection