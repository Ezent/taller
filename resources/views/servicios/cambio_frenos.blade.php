@extends('template.welcome')
@section('navbar')
@parent
@endsection
@section('carousel')
@endsection
@section('section1')
@endsection
@section('section2')
@endsection
@section('section3')
<section class="latest_news_area p_100">
    <div class="container">
        <div class="b_center_title">
            <h2>Cambio de Frenos</h2>
            <!--<p></p>-->
        </div>
        <div class="l_news_inner">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="l_news_item">
                        <div class="l_news_img"><a href="#"><p align='center'><img class="img-fluid img-serv" src="{{asset('img/servicios/cambio_frenos.jpg')}}" alt=""></p></a></div>
                        <div class="l_news_content">
                            <!--<a href="#"><h4>Soporte Magnético</h4></a>-->
                            <p class="text-justify">
                                Las pastillas de frenos son  parte de un sistema cuya misión sera la de reducir la velocidad o detener 
                                completamente el vehículo, las pastillas son un dispositivo que se fricciona junto con el disco a fin de 
                                lograr lo antes descrito, debido a esta fricción las pastillas de freno se desgastan siendo esto un proceso 
                                normal dentro del funcionamiento del sistema de frenos.
                            </p>
                            <p class="text-justify">
                                En realidad acá podemos hablar de tres alternativas que nos indican cuando cambiarlos:
                            </p>
                            <p class="text-justify">
                           .-  Revisión periódica del sistema de frenos: Esta alternativa seria la mas segura, el sistema de frenos esta sometido a muchos esfuerzos y temperatura, una revision cada 5 mil o 10 mil kilómetros como máximo nos asegura estar al tanto sobre el nivel de masa de frenado que le queda a la pastilla, ademas de esto se puede aprovechar para lubricar las mordazas y revisar las mangueras y discos de frenos.
                            </p>
                            <p class="text-justify">
                            .- Alerta  por medio del testigo metálico: En casi todas las pastillas de freno se provee de una alerta que consiste en un sensor metálico que no es mas que  una lata delgada metálica proyectada al disco cuyo largo concuerda con el grueso mínimo de la masa de la pastilla, esto significa que la masa nueva se ira desgastando y desgastando, cuando esta llegue a alcanzar un mínimo esta lata que esta proyectada hacia el disco empezara por tocar la cara del disco produciendo un sonido metálico muy agudo lo cual nos indicara que es hora de cambiar las pastillas.
                            </p>
                            <p class="text-justify">
                           .-  Alerta por medio de luz testigo en el tablero de instrumentos: Este tipo de alerta es muy parecida a la anterior con la diferencia que en la pastilla de frenos en vez de encontrar un sensor metálico que produce sonido encontramos un sensor metálico que envía una señal de tierra lo cual activa una luz de advertencia en el tablero de instrumentos, en este caso no se produce sonido alguno, solo se activa una luz testigo que nos indica un nivel bajo en las pastillas de freno.
                            </p>

                            <!--<a class="more_btn" href="#">Learn More</a>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

<!-- Pie de pagina por default-->
@section('section4')
@parent
@endsection
@section('footer')
@parent
@endsection