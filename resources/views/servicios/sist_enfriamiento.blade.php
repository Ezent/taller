@extends('template.welcome')
@section('navbar')
@parent
@endsection
@section('carousel')
@endsection
@section('section1')
@endsection
@section('section2')
@endsection
@section('section3')
<section class="latest_news_area p_100">
    <div class="container">
        <div class="b_center_title">
            <h2>Sistema de enfriamiento</h2>
            <!--<p></p>-->
        </div>
        <div class="l_news_inner">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="l_news_item">
                        <div class="l_news_img"><a href="#"><p align='center'><img class="img-fluid img-serv" src="{{asset('img/servicios/sist_enfriamiento.jpg')}}" alt=""></p></a></div>
                        <div class="l_news_content">
                            <!--<a href="#"><h4>Soporte Magnético</h4></a>-->
                            <p class="text-justify">
                                Gracias al sistem de enfriamiento, el motor de un  vehiculo se mantiene en la  temperatura óptima en todo momento, sin importar la circunstancia en la que trabaje,
                                de lo contrario de sobrecalentaria, gataría mas combustible y se dañarían sus componentes. Una maquina que se encuentra por debajo de los 80° deperdicia combustible 
                                y, si se calienta demasiado, los componentes de los motores corren el riesgo de fundirse y pegarse por lo que el sistema de enfriamiento es vital.
                            </p>
                            <!--<a class="more_btn" href="#">Learn More</a>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

<!-- Pie de pagina por default-->
@section('section4')
@parent
@endsection
@section('footer')
@parent
@endsection