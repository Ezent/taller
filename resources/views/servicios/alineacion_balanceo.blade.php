@extends('template.welcome')
@section('navbar')
@parent
@endsection
@section('carousel')
@endsection
@section('section1')
@endsection
@section('section2')
@endsection
@section('section3')
<section class="latest_news_area p_100">
    <div class="container">
        <div class="b_center_title">
            <h2>Alineacion y Balanceo</h2>
            <!--<p></p>-->
        </div>
        <div class="l_news_inner">
            <div class="row">
                <!--                        <div class="col-lg-2 col-md-2">
                                        </div>-->
                <div class="col-lg-12  col-md-12">
                    <div class="row l_news_item">
                        <div class="col-lg-12">
                            <div class="l_news_img"><a href="#"><img class="img-fluid" src="{{asset('img/servicios/alineacion.jpg')}}" alt=""></a></div>
                             <div class="l_news_content">
                                <a href="#"><h4>Alineacion y Balanceo</h4></a>
                                <p><b>Alineacion</b></p>
                                <p class="text-justify">
                                    Básicamente, la alineación es el proceso en el que se ajustan las llantas de un vehículo
                                    para que miren hacia el frente, es decir, las llantas de tu auto deben quedar paralelas 
                                    entre sí y perpendiculares al camino. Lo óptimo es realizar este servicio cada 10 mil kilómetros; 
                                    en caso de que no, tu volante tenderá a irse hacia un lado, o bien, nunca regresará a su posición 
                                    original después de girarlo, lo cual tarde o temprano generará un desgaste irregular.
                                </p>
                            <p><b>Balanceo</b></p>
                                <p>
                                    Existen dos tipos de balanceo: el estático y el dinámico. En el primero, se colocan pequeños 
                                    pesos en el rin para conseguir dicha estabilidad; y en el segundo, por el contrario, se toma
                                    en cuenta el peso de la llanta para realizar el ajuste correspondiente. Sabrás cuándo hay que
                                    balancear tus llantas cuando el volante de tu auto empiece a vibrar o este último genere ruidos 
                                    al circular a ciertas velocidades.
                                </p>
                                </div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

<!-- Pie de pagina por default-->
@section('section4')
@parent
@endsection
@section('footer')
@parent
@endsection