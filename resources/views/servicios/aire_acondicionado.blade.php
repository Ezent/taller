@extends('template.welcome')
@section('navbar')
@parent
@endsection
@section('carousel')
@endsection
@section('section1')
@endsection
@section('section2')
@endsection
@section('section3')
<section class="latest_news_area p_100">
    <div class="container">
        <div class="b_center_title">
            <h2>Aire acondicionado</h2>
            <!--<p></p>-->
        </div>
        <div class="l_news_inner">
            <div class="row">
                <div class="col-lg-12">
                    <div class="l_news_item">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="l_news_content">
                                    <!--<a href="#"><h4>Aire acondicionado</h4></a>-->
                                    <p class="text-justify">El aire acondicionado es el encargado de mantener nuestro espacio
                                        dentro del coche a una temperatura agradable, pero es importante darle cierto cuidado,
                                        hay que prestar la debida atención a su limpieza, lo que significa mantener limpio sobre
                                        todo el condensador. También es muy recomendable comprobar de vez en cuando la integridad de las 
                                        mangueras, ya que en caso de microroturas habrá fugas de gas refrigerante con la consiguiente 
                                        pérdida de la eficiencia de todo el aparato. Si no hay roturas, pero el aire acondicionado 
                                        no enfría bien esto significa que el nivel de gas es bajo y hay que recargarlo.
                                        También es necesario cambiar los filtros de forma periódica y evitar que el aparato trabaje 
                                        a máxima potencia: la temperatura ideal dentro del coche es de 22 grados. Además, a 
                                        temperaturas inferiores el consumo de combustible aumenta hasta un 20 %.</p>
                                    <!--<a class="more_btn" href="#">Learn More</a>-->
                                </div> 
                            </div>
                            <div class="col-lg-6">
                                <br>
                                <br>
                                <div class="l_news_img"><a href="#"><img class="img-fluid" src="{{asset('img/servicios/aire_acondicionado.jpg')}}" alt=""></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</section>
@endsection

<!-- Pie de pagina por default-->
@section('section4')
@parent
@endsection
@section('footer')
@parent
@endsection