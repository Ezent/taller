@extends('template.welcome')
@section('navbar')
@parent
@endsection
@section('carousel')
@endsection
@section('section1')
@endsection
@section('section2')
@endsection
@section('section3')
<section class="latest_news_area p_100">
    <div class="container">
        <div class="b_center_title">
            <h2>Hojalateria y Pintura</h2>
            <!--<p></p>-->
        </div>
        <div class="l_news_inner">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="l_news_item">
                        <div class="l_news_img"><a href="#"><p align='center'><img class="img-fluid img-carousel-responsive2" src="{{asset('img/servicios/hojalateria_pintura.jpg')}}" alt=""></p></a></div>
                        <div class="l_news_content">
                            <!--<a href="#"><h4>Soporte Magnético</h4></a>-->
                            <p class="text-justify">
                               Desde la creación del automóvil la pintura se usaba para decorar y embellecerlo, para darle un aspecto más atractivo. 
                               Pero esa no es la función principal de la pintura, ya que la más importante de todas es la prevención de corrosión (oxidación) al metal.
                            </p>
                            <p>
                                La hojalateria es el proceso de reparacion de rayones y golpes que el vehiculo haya sufrido,nosotros contamos con un excelente equipo de reparación
                                 con el que logramos evitar el cambio de piezas que por algún siniestro se hayan dañado generando grandes ahorros a nuestros clientes.
                            </p>
                            <!--<a class="more_btn" href="#">Learn More</a>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

<!-- Pie de pagina por default-->
@section('section4')
@parent
@endsection
@section('footer')
@parent
@endsection