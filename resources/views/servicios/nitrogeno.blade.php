@extends('template.welcome')
@section('navbar')
@parent
@endsection
@section('carousel')
@endsection
@section('section1')
@endsection
@section('section2')
@endsection
@section('section3')
<section class="latest_news_area p_100">
    <div class="container">
        <div class="b_center_title">
            <h2>Nitrógeno</h2>
            <!--<p></p>-->
        </div>
        <div class="l_news_inner">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="l_news_item">
                        <div class="l_news_img"><a href="#"><p align='center'><img class="img-fluid img-serv" src="{{asset('img/servicios/nitrogeno.jpg')}}" alt=""></p></a></div>
                        <div class="l_news_content">
                            <!--<a href="#"><h4>Soporte Magnético</h4></a>-->
                            <p class="text-justify">
                                Manejamos nitrogeno para tu automovil, con lo cual puedes obtener mejoras de 1 a 3 segundos y de 15 a 25 Km/h en el cuarto de milla.
                                Por supuesto, estos valores varirán de acuerdo al estado general y al tamaño del motor, las cubiertas, el sistema de transmisión, etc. 
                            </p>
                            <p>
                                <b>Existen tres tipos de Sistemas de Óxido Nitroso:</b>
                            </p>
                            <p>
                                <b>Sistema Seco.</b> 
                                Este sistema inyecta únicamente óxido nitroso en el conducto de admisión al ser accionado, y el aumento de
                                proporción de oxígeno se compensa con más combustible.  
                            </p>

                            <p>
                                <b> Sistema Húmedo. </b>
                                Este sistema es más complicado, ya que inyecta el óxido nitroso y el combustible a la vez, a través de una boquilla. 
                            </p>
                            <p>
                                <b>  Sistema de Puerto Directo. </b>
                                Este sistema añade el óxido nitroso y el combustible juntos a través de una manguera que, mezcla y mide la cantidad de 
                                ambos vertidos en cada cilindro. Este es el más potente que existe y uno de los más exactos, pero también es uno de 
                                los más complicados en lo que se refiere a su instalación, debido a esto y la gran potencia que desarrollan, son casi 
                                siempre utilizados en automóviles de carrera con motores preparados para soportar la carga de tales niveles de caballos de fuerza. 
                            </p>




                            <!--<a class="more_btn" href="#">Learn More</a>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

<!-- Pie de pagina por default-->
@section('section4')
@parent
@endsection
@section('footer')
@parent
@endsection