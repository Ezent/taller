@extends('template.welcome')
@section('navbar')
@parent
@endsection
@section('carousel')
@endsection
@section('section1')
@endsection
@section('section2')
@endsection
@section('section3')
<section class="latest_news_area p_100">
    <div class="container">
        <div class="b_center_title">
            <h2>Cambio de aceite</h2>
            <!--<p></p>-->
        </div>
        <div class="l_news_inner">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="l_news_item">
                        <div class="l_news_img"><a href="#"><img class="img-fluid img-carousel-responsive2" src="{{asset('img/servicios/cambio_aceite.jpg')}}" alt=""></a></div>
                        <div class="l_news_content">
<!--                            <a href="#"><h4>Soporte Magnético</h4></a>-->
                            <p class="text-justify">
                                Un aspecto  importante  para mantener un vehículo en perfectas condiciones, es el cambio de aceite,
                                ya que éste se encarga de lubricar el motor, además de protegerlo y asegurar su buen funcionamiento.
                                De lo contrario, el contacto de metal con metal podría causar daños graves a la máquina y poner en riesgo 
                                la seguridad del conductor y sus acompañantes. 
                            </p>
                            <p>Algunos consejos para el cambio de aceite</p>
                            <ul style="text-align: justify">
                                <li>
                                    •  Siempre comprueba en el manual del vehículo cuál es el aceite más adecuado para el motor y su frecuencia de renovación. 
                                </li>
                                <li>
                                    •  Aunque existen muchas consideraciones a tomar en cuenta, como hábitos de conducción, tipos de vehículo, aceite a utilizar, entre otros,
                                    algo elemental que no debes olvidar es evitar superar los 20.000 kilómetros con el mismo aceite, del tipo que sea.
                                </li>
                                <li>
                                    •  Te sugerimos hacer el cambio de aceite al menos dos veces al año, así asegurarás la eficiencia y rendimiento óptimo de la máquina. 
                                </li>
                                <li>
                                    •  Recuerda revisar el nivel del lubricante todas las semanas y agregar aceite si es necesario. También asegúrate de rellenar el depósito 
                                    con un producto que sea de las mismas características que el anterior.
                                </li>
                                <li>
                                    •  Utiliza un aceite de alta calidad para garantizar el rendimiento de la máquina. TOTAL cuenta con una gran variedad de productos
                                    especializados que se adaptan a las características de cada tipo de automóvil o transporte. 
                                </li>
                                <li>
                                    • Lo más recomendable es que hagas  el cambio de aceite en un taller,  porque cuentan con las herramientas adecuadas para realizarlo,
                                    aunque recuerda  que también lo puedes hacer tú mismo; tú elige, de ser así ten a la mano: Elevador, guantes, embudo y recipientes
                                    necesarios; llave ajustable para el filtro de aceite, llaves fijas, filtro de aceite nuevo y la cantidad de producto necesaria. 
                                </li>
                                <li>
                                    •  Busca un lugar seguro para deshacerte del aceite viejo, existen depósitos especiales para ello. Recuerda que éste es un residuo
                                    que se puede reciclar. 
                                </li>
                            </ul>

                            <p>
                                No olvides que el cuidado del auto es fundamental para tu seguridad y la del vehículo. Por eso es importante que 
                                tengas el hábito de controlar los puntos que te acabamos de mencionar y así prevengas incidentes.   
                            </p>
                            <!--<a class="more_btn" href="#">Learn More</a>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

<!-- Pie de pagina por default-->
@section('section4')
@parent
@endsection
@section('footer')
@parent
@endsection