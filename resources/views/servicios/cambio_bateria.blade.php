@extends('template.welcome')
@section('navbar')
@parent
@endsection
@section('carousel')
@endsection
@section('section1')
@endsection
@section('section2')
@endsection
@section('section3')
<section class="latest_news_area p_100">
    <div class="container">
        <div class="b_center_title">
            <h2>Cambio y revisión de bateria</h2>
            <!--<p></p>-->
        </div>
        <div class="l_news_inner">
            <div class="row">
                <div class="col-lg-12 col-md-12">
                      <div class="l_news_item">
                    <div class="row">
                        <div class="col-lg-4">
                            <br>
                            <br>
                        <div class="l_news_img"><a href="#"><img class="img-fluid" src="{{asset('img/servicios/cambio_bateria.jpg')}}" alt=""></a></div>
                        </div>
                        <div class="col-lg-8">
                             <div class="l_news_content">
                            <!--<a href="#"><h4>GPS</h4></a>-->
                            <p class="text-justify">
                                Para conservar mejor tu batería y garantizar tu seguridad, te recomendamos:
                            <ul>
                                <li>
                                    - Controlar regularmente tu batería cada 30.000 km o cada 2 años
                                </li>
                                <li>
                                    - Cambiar tu batería cada 60.000 km o cada 4 años
                                </li>
                                <li>
                                    - Controlar el circuito de arranque antes del verano e invierno para evitar averías causadas 
                                    por el cambio de temperatura Puedes verificar tú mismo el nivel de carga de tu batería gracias
                                    al testigo situado en ella.
                                    Cada mañana, enciende durante 4 o 5 segundos los faros de tu vehículo antes de arrancar.
                                    Así proporcionarás a tu batería energía para el arranque y te durará más tiempo.
                                </li>
                            </ul>
                            </p>
                            <!--<a class="more_btn" href="#">Learn More</a>-->
                        </div>
                        </div>
                    </div>
                          </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

<!-- Pie de pagina por default-->
@section('section4')
@parent
@endsection
@section('footer')
@parent
@endsection