<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-4">
                    <div class="card" style="width: 30rem; background: steelblue;" >
<!--                        <p align="center"> <img class="img-fluid" width="50%" src="#" ></p>-->
                        Talleres | Espinosa Trucks
                        <div class="card-body" style="background: #ECEAEA">
                            <h5 class="card-title">Correo de Cliente</h5>
                            <br>
                            <b>Nombre del Cliente:</b>
                            <p class="card-text">{{$nombre}} </p>
                            <b>Correo de respuesta:</b>
                            <p class="card-text">{{$correo}}</p>
                            <br>
                            <b>Asunto:</b>
                            <p class="card-text">{{$asunto}}</p>
                            <b>Mensaje:</b>
                            <p class="card-text">{{$mensaje}}</p>
                            <br>
                            <!--<a class="btn btn-primary"><img src="" height="30px"></a>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
