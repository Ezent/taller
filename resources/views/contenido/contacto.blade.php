@extends('template.welcome')
@section('navbar')
@parent
@endsection
@section('carousel')
<!--Seccion de contacto-->
<section class="contact_us_area">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="l_news_item">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1862.1644757823494!2d-89.6435362!3d21.0195199!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMjHCsDAxJzExLjIiTiA4OcKwMzgnMzQuOCJX!5e0!3m2!1ses!2smx!4v1544898444167"
                            width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div class="contact_details_inner">
            <div class="row">
                <div class="col-lg-6">
                    <div class="contact_text">
                        <div class="main_title text-justify" >
                            <h2>Información de contacto</h2>
                            <p>Deseas acudir a nuestras instalaciones o ponerte en contacto vía telefónica, dejamos nuestros datos de contacto para dar respuestas a tus preguntas!!!</p>
                        </div>
                        <div class="contact_d_list">
                            <div class="contact_d_list_item">
                                Calle 30 #281 por 15, Chuburna Hidalgo, 
                                cp. 97205, Merida, Mex.
                            </div>
                            <div class="contact_d_list_item">
                                <p>Tel: 999 194 2844 <br /> Gerente. Francisco Javier Espinosa</p>
                            </div>
                            <div class="contact_d_list_item">
                                <p>Abierto: 8.00-18.00 Lunes-Sabado <br />Domingos: Cerrado</p>
                            </div>
                        </div>
<!--                        <div class="static_social">
                            <div class="main_title">
                                <h2>Redes Sociales</h2>
                            </div>
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>-->
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="contact_form">
                        <div class="main_title">
                            <h2>Envianos un mensaje</h2>
                            <p>Tienes dudas, deseas cotizar algun servicio o quieres algo en especial, no dudes y envianos un mensaje</p>
                        </div>
                        <form onsubmit="ajaxSend(); return false;" class="contact_us_form row" action="{{asset(route('sendEmail'))}}" method="post" id="contactForm" >
                            {{csrf_field()}}
                            <div class="form-group col-lg-12">
                                <input required id="nombre" type="text" class="form-control"  name="nombre" placeholder="Nombre Completo">
                            </div>
                            <div class="form-group col-lg-12">
                                <input required id="correo" type="email" class="form-control" name="correo" placeholder="Correo electronico">
                            </div>
                            <div class="form-group col-lg-12">
                                <input required id="asunto" type="text" class="form-control"  name="asunto" placeholder="Asunto">
                            </div>
                            <div class="form-group col-lg-12">
                                <textarea required id="mensaje" class="form-control" name="mensaje" rows="10" placeholder="Escribe aqui tu mensaje"></textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <button type="submit"  class="btn submit_btn2 form-control">Enviar Mensaje</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Final de la seccion de contacto-->
<script type="text/javascript">
    function ajaxSend() {
        let form=$('#contactForm');
        let url=form.attr('action');
        let data=form.serialize();
        $.post(url,data, function(msj){
            /*si el correo se envia borramos las etiquetas de entrada*/
            $('#nombre').val('');
            $('#asunto').val('');
            $('#correo').val('');
            $('#mensaje').val('');
            alert(msj);
        }).fail(function(error,status, errorThrown){
            alert(status+': '+errorThrown);
        });
    }
</script>
@endsection
@section('section1')
@endsection
@section('section2')
@endsection
@section('section3')
@endsection

<!-- Pie de pagina por default-->
@section('section4')
@parent
@endsection
@section('footer')
@parent
@endsection
