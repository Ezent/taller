<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Taller mecanico automotriz">
        <meta name="keywords" content="Taller mecanico automotriz, Taller mecanico Truks, Taller mecanico merida">
        <meta name="author" content="Taller Truks">
        <title>Taller | Espinosa Trucks</title>
        <link rel="stylesheet"  href="{{asset('bootstrap_4.1/css/app.css')}}"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <!--Estilo personal-->
        <link rel="stylesheet"  href="{{asset('bootstrap_4.1/css/my-style.css')}}"/>
        <script type="text/javascript"  src="{{asset('bootstrap_4.1/js/app.js')}}"></script>
        <style>
            .ajustar{
                height: 80px;
            }
            .espacio{
                height: 40px;
            }
            @media(max-width:620px){
                .espacio{
                    height: 60px;
                }
                .ajustar{
                    height: 0;
                }
            }
        </style>
    </head>
    <body>
        @section('navbar')
        <header>
            <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                <div class="col-md-2"></div>
                <a class="navbar-brand" href="/">Taller | Espinosa Trucks</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="col-md-2"></div>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item {{(Request::is('/')) ? 'active' : ''}}">
                            <a class="nav-link" href="/">Inicio <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item dropdown {{(Request::is('contacto') | Request::is('/'))?'':'active'}}" >
                            <a class="nav-link dropdown-toggle" href="#"  id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Servicios
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="/accesorios"> Accesorios</a>
                                <a class="dropdown-item" href="/alineacion&balanceo">Alineacion y Balanceo</a>
                                <a class="dropdown-item" href="/aire_acondicionado"> Aire acondicionado</a>
                                <a class="dropdown-item" href="/cambio_bateria"> Cambio y Revision de Bateria</a>
                                <a class="dropdown-item" href="/cambio_aceite"> Cambio de Aceite</a>
                                <a class="dropdown-item" href="/cambio_frenos"> Cambio de Frenos</a>
                                <a class="dropdown-item" href="/hojalateria&pintura"> Hojalateria y Pintura</a>
                                <a class="dropdown-item" href="/nitrogeno"> Nitrogeno</a>
                                <a class="dropdown-item" href="/sistema_enfriamiento"> Sistema de enfriamiento</a>
                            </div>
                        </li>
                        <li class="nav-item  {{(Request::is('contacto')) ? 'active' : ''}}">
                            <a class="nav-link" href="{{route('contacto')}}">Contacto</a>
                        </li>
                    </ul>
                    <!--                    <form class="form-inline mt-2 mt-md-0">
                                            <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                                            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                                        </form>-->
                </div>
            </nav>
        </header>
@show
        <!-- Agregar contenido-->
        <main role="main">
            @section('carousel')
            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="first-slide img-carousel-responsive sombra" src="{{asset('img/t1.jpg')}}" alt="First slide">
                        <div class="container">
                            <div class="carousel-caption text-right">
                                <h1>El mejor cuidado para tu auto.</h1>
                                <p><b>Taller Espinosa Trucks</b> se preocupa por el bienestar de tu auto, te ofrecemos el mejor servicio de gran calidad automotriz</p>
                                <!--<p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>-->
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="second-slide img-carousel-responsive sombra"  src="{{asset('img/t2.jpg')}}"  alt="Second slide">
                        <div class="container">
                            <div class="carousel-caption">
                                <h1>Excelentes Instalaciones</h1>
                                <p>Nuestras instalaciones cuentan con el mejor equipo para atender tu automovil en cualquier circunstancia.</p>
                                <!--<p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>-->
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="third-slide img-carousel-responsive sombra" src="{{asset('img/t3.jpg')}}"  alt="Third slide">
                        <div class="container">
                            <div class="carousel-caption text-left">
                                <h1>Personal altamente capacitado.</h1>
                                <p>Nuestro personal cuenta con una gran experiencia en mantenimiento y reparacion de vehiculos</p>
                                <!--<p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>-->
                            </div>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            @show
            <div class="ajustar"></div>
            @include('template.plantilla')
            <!-- código HTMl botón subir (top)-->
            <a class="ir-arriba"  href="#" title="Volver arriba">
                <span class="fa-stack">
                    <i class="fa fa-circle fa-stack-2x"></i>
                    <i class="fa fa-arrow-up fa-stack-1x fa-inverse"></i>
                </span>
            </a>
        </main>
        <!--Fin de contenido -->

        <!-- script para subir al top -->
        <script>
$(document).ready(function () {

    /*********************************************** boton hacia arriba **********************************************/
    $('.ir-arriba').click(function () {
        $('body, html').animate({
            scrollTop: '0px'
        }, 1000);
    });

    $(window).scroll(function () {
        if ($(this).scrollTop() > 0) {
            $('.ir-arriba').slideDown(600);
        } else {
            $('.ir-arriba').slideUp(600);
        }
    });

    /*hacia abajo*/
    $('.ir-abajo').click(function () {
        $('body, html').animate({
            scrollTop: '1000px'
        }, 1000);
    });

});
        </script>
        <!--Fin del top  -->
    </body>
</html>
