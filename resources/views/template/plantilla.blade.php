<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Icon css link -->
        <link href="{{asset('template/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{asset('template/vendors/elegant-icon/style.css')}}" rel="stylesheet">
        <link href="{{asset('template/vendors/themify-icon/themify-icons.css')}}" rel="stylesheet">
        <!-- Bootstrap -->

        <!-- Rev slider css -->
        <link href="{{asset('template/vendors/revolution/css/settings.css')}}" rel="stylesheet">
        <link href="{{asset('template/vendors/revolution/css/layers.css')}}" rel="stylesheet">
        <link href="{{asset('template/vendors/revolution/css/navigation.css')}}" rel="stylesheet">
        <link href="{{asset('template/vendors/animate-css/animate.css')}}" rel="stylesheet">

        <!-- Extra plugin css -->
        <link href="{{asset('template/vendors/owl-carousel/owl.carousel.min.css')}}" rel="stylesheet">

        <link href="{{asset('template/css/style.css')}}" rel="stylesheet">
        <link href="{{asset('template/css/responsive.css')}}" rel="stylesheet">

    </head>
    <body>
        <!--================Creative Feature Area =================-->
        @section('section1')
        <section class="creative_feature_area">
            <div class="container">
                <div class="c_feature_box">
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="c_box_item">
                                <a href="#"><h4><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> Confianza</h4></a>
                                <p>Ofrecemos la mayor confianza a nuestros clientes ya que todos nuestros servicios van probados y garantizados delante el cliente y por escrito.</p>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="c_box_item">
                                <a href="#"><h4><i class="fa fa-clock-o" aria-hidden="true"></i> Experiencia</h4></a>
                                <p>Somos un taller mecanico en donde nuestra experiencia siempre se ha basado en la fislosofía que va más haya de hacer bien las cosas,
                                    en la que nuestro clientes hablen por ello y nos brinden su absoluta confianza.</p>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="c_box_item">
                                <a href="#"><h4><i class="fa fa-diamond" aria-hidden="true"></i> Calidad</h4></a>
                                <p>Todos nuestros servicios se hacen de la mejor manera posible, no solo nos enfocamos en que funcione correctamente si no que se vea funcionalmente bien.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="digital_feature p_100">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="d_feature_text">
                                <div class="main_title">
                                    <h2>¿Quiénes somos?<br /> Conocenos!!!</h2>
                                </div>
                                <p>
                                    Somos un Taller mecánico automotriz que cuenta con la capacitación y equipamiento necesario para atender todo tipo de vehiculo de cualquier marca,
                                    nos aseguramos de hacer bien nuestro trabajo y de que nuestros clientes queden completamente satisfechos.

                                </p>
                                <!--                                <a class="read_btn" href="#">Read more</a>-->
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="d_feature_img">
                                <img class="img-fluid" src="img/t1_1.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @show
        @section('section2')
        <section class="service_area">
            <div class="container">
                <div class="center_title">
                    <h2>Nuestros Servicios</h2>
                    <p>Garantizamos nuestro servicios y los ponemos a tu disposición</p>
                </div>
                <div class="row service_item_inner">
                    <div class="col-lg-4">
                        <div class="service_item">
                            <i class="ti-pencil"></i>
                            <i class="ti-book"></i>
                            <h4>Garantía por escrito</h4>
                            <p>Nuestro servicios son de calidad y nos aseguramos ofreciendote garantía por escrito </p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="service_item">
                            <i class="ti-desktop"></i>
                            <h4>Asistencia en linea</h4>
                            <p>Ponemos a tu dispocisión nuestro servicio de soporte en linea para cotizar materiales de repuesto o asesoria</p>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="service_item">
                            <i class="ti-bookmark"></i>
                            <h4>Pago con tarjeta</h4>
                            <p>Para una mayor comodidad aceptamos pago con tarjetas de crédito o debito</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @show
        <!--================End Our Service Area =================-->

        <!--================Testimonials Area =================-->
<!--        <section class="testimonials_area p_100">
            <div class="container">
                <div class="testimonials_slider owl-carousel">
                    <div class="item">
                        <div class="media">
                            <img class="d-flex rounded-circle" src="img/testimonials-1.png" alt="">
                            <div class="media-body">
                                <img src="{{asset('img/g1.jpg')}}" alt="">
                                <p>Cambio de llantas</p>
                                <h4><a href="#">Aigars Silkalns</a> - CEO DeerCreative</h4>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="media">
                            <img class="d-flex rounded-circle" src="img/testimonials-1.png" alt="">
                            <div class="media-body">
                                <img src="img/dotted-icon.png" alt="">
                                <p>I wanted to mention that these days, when the opposite of good customer and tech support tends to be the norm, it’s always great having a team like you guys at Fancy! So, be sure that I’ll always spread the word about how good your product is and the extraordinary level of support that you provide any time there is any need for it.</p>
                                <h4><a href="#">Aigars Silkalns</a> - CEO DeerCreative</h4>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="media">
                            <img class="d-flex rounded-circle" src="img/testimonials-1.png" alt="">
                            <div class="media-body">
                                <img src="img/dotted-icon.png" alt="">
                                <p>I wanted to mention that these days, when the opposite of good customer and tech support tends to be the norm, it’s always great having a team like you guys at Fancy! So, be sure that I’ll always spread the word about how good your product is and the extraordinary level of support that you provide any time there is any need for it.</p>
                                <h4><a href="#">Aigars Silkalns</a> - CEO DeerCreative</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>-->
        <!--================End Testimonials Area =================-->
        <!--================Latest News Area =================-->
        @section('section3')
        <section class="latest_news_area p_100">
            <div class="container">
                <div class="b_center_title">
                    <h2>Noticias</h2>
                    <p>A continuacion te brindamos algunos tips para cuidar tu automovil</p>
                </div>
                <div class="l_news_inner">
                    <div class="row">
                        <div class="col-lg-4 col-md-6">
                            <div class="l_news_item">
                                <div class="l_news_img"><a href="#"><img class="img-fluid" src="{{asset('img/g2.jpg')}}" alt=""></a></div>
                                <div class="l_news_content">
                                    <a href="#"><h4>Suspensiones</h4></a>
                                    <p class="text-justify">La Suspension es importante porque permite:</p>
                                    Absorber las desigualdades de los terrenos por los que se desplaza el coche,
                                    Evitar la inclinación excesiva de la parte delantera en el frenado,
                                    Dar confort y seguridad durante la marcha y proteger las piezas del auto</p>
                                    <!--<a class="more_btn" href="#">Learn More</a>-->
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="l_news_item">
                                <div class="l_news_img"><a href="#"><img class="img-fluid" src="{{asset('img/g3.jpg')}}" alt=""></a></div>
                                <div class="l_news_content">
                                    <a href="#"><h4>Pintura</h4></a>
                                    <p class="text-justify">
                                        La pintura automotriz es un trabajo que se realiza en todo el mundo, tanto en las fábricas automotrices como en los talleres de reparación.
                                        Desde la creación del automóvil la pintura se usaba para decorar y embellecerlo, para darle un aspecto más atractivo.
                                        Pero esa no es la función principal de la pintura, ya que la más importante de todas es la prevención de corrosión (oxidación) al metal.
                                    </p>
                                    <!--<a class="more_btn" href="#">Learn More</a>-->
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6">
                            <div class="l_news_item">
                                <div class="l_news_img"><a href="#"><img class="img-fluid" src="{{asset('img/g1.jpg')}}" alt=""></a></div>
                                <div class="l_news_content">
                                    <a href="#"><h4>Rines</h4></a>
                                    <p class="text-justify">Los rines no sólo hacen que las llantas de tu auto luzcan bien en el exterior, también contribuyen a la resistencia
                                        aerodinámica del vehículo y su peso puede hacer mucha diferencia en la conducción.</p>
                                    <p class="text-justify"> Por eso no es casualidad que los
                                        fabricantes pongan mucha atención en el diseño y los materiales con los que se hacen.</p>
                                    <!--<a class="more_btn" href="#">Learn More</a>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @show
        <!--================End Latest News Area =================-->
        @section('section4')
        <section class="project_area">
            <div class="container">
                <div class="project_inner">
                    <div class="center_title">
                        <h2>Quiéres contactarnos? </h2>
                        <p>Tienes alguna duda o deseas preguntar por algunos de nuestros servicios, ponte en contacto con nosotros.</p>
                    </div>
                    <a class="tp_btn" href="/contacto">Contactar</a>
                </div>
            </div>
        </section>
        @show
        <!--================Footer Area =================-->
        @section('footer')
        <footer class="footer_area">
            <!--            <div class="footer_widgets_area">
                            <div class="container">
                                <div class="f_widgets_inner row">
                                    <div class="col-lg-3 col-md-6">
                                        <aside class="f_widget subscribe_widget">
                                            <div class="f_w_title">
                                                <h3>Our Newsletter</h3>
                                            </div>
                                            <p>Subscribe to our mailing list to get the updates to your email inbox.</p>
                                            <div class="input-group">
                                                <input type="email" class="form-control" placeholder="E-mail" aria-label="E-mail">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-secondary submit_btn" type="button">Subscribe</button>
                                                </span>
                                            </div>
                                            <ul>
                                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                            </ul>
                                        </aside>
                                    </div>
                                    <div class="col-lg-3 col-md-6">
                                        <aside class="f_widget twitter_widget">
                                            <div class="f_w_title">
                                                <h3>Twitter Feed</h3>
                                            </div>
                                            <div class="tweets_feed"></div>
                                        </aside>
                                    </div>
                                    <div class="col-lg-3 col-md-6">
                                        <aside class="f_widget categories_widget">
                                            <div class="f_w_title">
                                                <h3>Link Categories</h3>
                                            </div>
                                            <ul>
                                                <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Agency</a></li>
                                                <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Studio</a></li>
                                                <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Studio</a></li>
                                                <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Blogs</a></li>
                                                <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Shop</a></li>
                                            </ul>
                                            <ul>
                                                <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Home</a></li>
                                                <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>About</a></li>
                                                <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Services</a></li>
                                                <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Work</a></li>
                                                <li><a href="#"><i class="fa fa-angle-double-right" aria-hidden="true"></i>Privacy</a></li>
                                            </ul>
                                        </aside>
                                    </div>
                                    <div class="col-lg-3 col-md-6">
                                        <aside class="f_widget contact_widget">
                                            <div class="f_w_title">
                                                <h3>Contact Us</h3>
                                            </div>
                                            <a href="#">1 (800) 686-6688</a>
                                            <a href="#">info.deercreative@gmail.com</a>
                                            <p>40 Baria Sreet 133/2 <br />NewYork City, US</p>
                                            <h6>Open hours: 8.00-18.00 Mon-Fri</h6>
                                        </aside>
                                    </div>
                                </div>
                            </div>
                        </div>-->
            <div class="copy_right_area">
                <div class="container">
                    <div class="float-md-left">
                        <h5>Taller Espinosa Trucks | Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved </h5>
                    </div>
                    <div class="float-md-right">
                        <ul class="nav" >
                            <li class="nav-item" ><a class="nav-link" href="#"><i class="fa fa-facebook"></i></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-twitter"></i></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-instagram"></i></a></li>
                            <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
        @show
        <!--================End Footer Area =================-->




        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <!--<script src="template/js/jquery-3.2.1.min.js"></script>-->
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="{{asset('template/js/popper.min.js')}}"></script>
        <!--<script src="template/js/bootstrap.min.js"></script>-->
        <!-- Rev slider js -->
        <script src="{{asset('template/vendors/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
        <script src="{{asset('template/vendors/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
        <script src="{{asset('template/vendors/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
        <script src="{{asset('template/vendors/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>
        <script src="{{asset('template/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
        <script src="{{asset('template/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
        <script src="{{asset('template/vendors/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
        <script src="{{asset('template/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
        <!-- Extra plugin css -->
        <script src="{{asset('template/vendors/counterup/jquery.waypoints.min.js')}}"></script>
        <script src="{{asset('template/vendors/counterup/jquery.counterup.min.js')}}"></script>
        <script src="{{asset('template/vendors/counterup/apear.js')}}"></script>
        <script src="{{asset('template/vendors/counterup/countto.js')}}"></script>
        <script src="{{asset('template/vendors/owl-carousel/owl.carousel.min.js')}}"></script>
        <script src="{{asset('template/vendors/parallaxer/jquery.parallax-1.1.3.js')}}"></script>
        <!--Tweets-->
        <script src="{{asset('template/vendors/tweet/tweetie.min.js')}}"></script>
        <script src="{{asset('template/vendors/tweet/script.js')}}"></script>

        <script src="{{asset('template/js/theme.js')}}"></script>
    </body>
</html>
