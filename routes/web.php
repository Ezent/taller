<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('template.welcome');
});
/*rutas de los servicios*/
Route::get('/accesorios', function () {
    return view('servicios.accesorios');
});
Route::get('/alineacion&balanceo', function () {
    return view('servicios.alineacion_balanceo');
});
Route::get('/aire_acondicionado', function () {
    return view('servicios.aire_acondicionado');
});
Route::get('/cambio_bateria', function () {
    return view('servicios.cambio_bateria');
});
Route::get('/cambio_aceite', function () {
    return view('servicios.cambio_aceite');
});
Route::get('/cambio_frenos', function () {
    return view('servicios.cambio_frenos');
});
Route::get('/hojalateria&pintura', function () {
    return view('servicios.hojalateria_pintura');
});
Route::get('/nitrogeno', function () {
    return view('servicios.nitrogeno');
});
Route::get('/sistema_enfriamiento', function () {
    return view('servicios.sist_enfriamiento');
});


/*contactos*/
Route::get('/contacto','contacto\ContactoController@viewContacto')->name('contacto');
Route::post('/sendEmail','contacto\ContactoController@send')->name('sendEmail');

/*pagina de pruebas para enviuo de correo*/
Route::get('/pageEmail',function (){
    return view('mail.mail-send');
})->name('pruebas');

/*Pruebas*/
Route::get('/plantilla', function () {
    return view('template.plantilla');
});
